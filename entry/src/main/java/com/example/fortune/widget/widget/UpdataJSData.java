package com.example.fortune.widget.widget;

import com.zzrv5.mylibrary.ZZRCallBack;
import com.zzrv5.mylibrary.ZZRHttp;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.utils.zson.ZSONObject;
import java.util.HashMap;
import java.util.Map;

public class UpdataJSData {
    private static final HiLogLabel TAG = new HiLogLabel(HiLog.DEBUG, 0x0, UpdataJSData.class.getName());
    public static void getJsonData(String constellation, ZZRCallBack.CallBackString callBackName ) {
        String url = "http://web.juhe.cn:8080/constellation/getAll";
        Map<String,String> map = new HashMap<>();
//        map.put("key", "d7f0ca5eef79ce9ac6a9ec3838b3fd4d");
        map.put("key", "9d075905a3e1c33224ac5bb42f4de458");
        map.put("consName", constellation);
        map.put("type", "today");
        ZZRHttp.get(url, map, callBackName);
    }
    public static ZSONObject setJson(String response) {
        ZSONObject fortuneData = ZSONObject.stringToZSON(response);
        HiLog.info(TAG,"数据为"+fortuneData);
        int error_code = fortuneData.getIntValue("error_code");
        if (error_code == 0) {
            HiLog.info(TAG,"返回的数据正确,设置js中的json数据");
            String[][] all = formatConversion(fortuneData.getString("all"));
            String[][] love = formatConversion(fortuneData.getString("love"));
            String[][] money = formatConversion(fortuneData.getString("money"));
            String[][] work = formatConversion(fortuneData.getString("work"));
            String[][] health = formatConversion(fortuneData.getString("health"));
            HiLog.info(TAG,health.toString());
            fortuneData.put("imgSrc", "/common/constellation/" + fortuneData.getString("name").toString().substring(0, 2) + ".jpg");// 星座图片路径拼接
            fortuneData.put("conName", fortuneData.getString("name"));// 星座名称
            fortuneData.put("conDate", fortuneData.getString("datetime"));// 当前事件
            fortuneData.put("conPari", fortuneData.getString("QFriend"));// 速配星座
            fortuneData.put("conNumber", fortuneData.getString("number"));// 幸运数组
            fortuneData.put("conColor", fortuneData.getString("color"));// 幸运颜色
            fortuneData.put("conSummary", fortuneData.getString("summary"));// 描述信息
            fortuneData.put("conAllStarY", all[0]);// 综合评分 标记星
            fortuneData.put("conAllStarN", all[1]);// 综合评分 未标记星
            fortuneData.put("conLoveStarY", love[0]);// 爱情评分 标记星
            fortuneData.put("conLoveStarN", love[1]);// 爱情评分 未标记星
            fortuneData.put("conMoneyStarY", money[0]);// 理财评分 标记星
            fortuneData.put("conMoneyStarN", money[1]);// 理财评分 未标记星
            fortuneData.put("conWorkStarY", work[0]);// 工作评分 标记星
            fortuneData.put("conWorkStarN", work[1]);// 工作评分 未标记星
            fortuneData.put("conHealthStarY", health[0]);// 健康评分 标记星
            fortuneData.put("conHealthStarN", health[1]);// 健康评分 未标记星
        } else {
            HiLog.info(TAG,"请求出错，code=" + error_code);
        }
        return fortuneData;
    }
    private static String[][] formatConversion(String star) {
        int myData = Integer.parseInt(star);
        int myDataY = myData / 20;
        int myDataN = 5 - myDataY;
        HiLog.info(TAG,"star=" + star + "myDataY=" + myDataY + "      myDataN=" + myDataN);
        String myListY[] = new String[myDataY];
        String myListN[] = new String[myDataN];
        for(int i = 0; i < myDataY; i++) {
            myListY[i] = "Y";
        }
        for(int i = 0; i < myDataN; i++) {
            myListN[i] = "N";
        }
        String[][] myList = {myListY, myListN};
        return myList;
    }
}