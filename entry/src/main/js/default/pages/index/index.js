import http from '@ohos.net.http';// 导入http
export default {
    data: {
        conName: "天蝎座",
        options: {
            key: "9d075905a3e1c33224ac5bb42f4de458",
            type: "today"
        },
        url: "",
        conData: {
            name: "天蝎座",
            datetime: "2021年7月28号",
            img: "/common/images/constellation/天蝎.jpg",
            QFriend: "金牛",
            number: "4",
            color: "粉色",
            summary: "有些思考的小漩涡，可能让你忽然的放空，生活中许多的细节让你感触良多，五味杂陈，常常有时候就慢动作定格，想法在某处冻结停留，陷入一阵自我对话的沉思之中，这个时候你不喜欢被打扰或询问，也不想让某些想法曝光，个性变得有些隐晦。"
        },
        responseData: {}
    },
    onInit() {
        this.getHttp();// 发送请求
    },
//    输入框改变触发
    changeCon(v) {
        this.conName = v.value;
        console.log(v.value+ 'change conName');
    },
    getHttp() {
        this.url = `http://web.juhe.cn:8080/constellation/getAll?key=${this.options.key}&consName=${this.conName}&type=${this.options.type}`
        let httpRequest = http.createHttp();
        httpRequest.request(this.url, {
          method: 'GET'
        },(err, data) => {
          if (err == null) {
            let conData = JSON.parse(data.result);
            console.log('Result:' + data.result);
            if (conData.error_code == 0) {
                conData.img = `/common/images/constellation/${(conData.name).slice(0, 2)}.jpg`
                conData.allY = this.getNewArr(conData.all, "Y");
                conData.allN = this.getNewArr(conData.all, "N");
                conData.healthY = this.getNewArr(conData.health, "Y");
                conData.healthN = this.getNewArr(conData.health, "N");
                conData.loveY = this.getNewArr(conData.love, "Y");
                conData.loveN = this.getNewArr(conData.love, "N");
                conData.moneyY = this.getNewArr(conData.money, "Y");
                conData.moneyN = this.getNewArr(conData.money, "N");
                conData.workY = this.getNewArr(conData.work, "Y");
                conData.workN = this.getNewArr(conData.work, "N");
                this.conData = conData;
            } else {
                console.error("请求失败");
            }
          } else {
            console.log('error:' + JSON.stringify(err));
          }
        });
    },
//    数字转化为数组不然for循环不出来
    getNewArr(score, type){
        score = parseInt(parseInt(score) / 20);
        let arr = [];
        if (type === "N") {
            console.log('type' + type)
            score = 5 - score;
            for (var i = 0; i < score; i++) {
                arr.push(type);
            }
        } else {
            for (var i = 0; i < score; i++) {
                arr.push(type);
            }
        }
        return arr;
    }
}
